package djf.components;

import java.util.ArrayList;

/**
 * This interface provides the structure for data components in
 * our applications.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public interface AppDataComponent {

    /**
     * This function would be called when initializing data.
     */
    public void resetData();
    public ArrayList getTempTas();
    public int getStartHour();
    public int getEndHour();

}
