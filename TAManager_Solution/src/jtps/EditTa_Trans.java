/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.TempTa;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tam.TAManagerProp;
import static tam.style.TAStyle.CLASS_ADD_TA_BUTTON;
import java.util.List;
import jtps.jTPS_Transaction;
import tam.data.TempTa;
import tam.workspace.TAWorkspace;
//import jtps.jTPS;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Hengqi Zhu
 */
public class EditTa_Trans implements jtps.jTPS_Transaction{
    String oldname;
    String oldemail;
    String newname;
    String newemail;
    TAManagerApp app;
    
    public EditTa_Trans(TAManagerApp app, String on,String oe,String nn,String ne){
        this.oldname=on;
        this.oldemail=oe;
        this.newname=nn;
        this.newemail=ne;
        this.app=app;
    
    }
   
    

    @Override
    public void doTransaction() {
        
         TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
         TableView taTable = workspace.getTATable();
        TAData data = (TAData)app.getDataComponent();
       data.updateTA(oldname, newname, newemail);    
            
            // CLEAR THE TEXT FIELDS
           
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(oldname)
                    || (label.getText().contains(oldname + "\n"))
                    || (label.getText().contains("\n" + oldname))) {
                        data.updataTAFromCell(label.textProperty(), oldname,newname);
                    }
                }
                
                taTable.refresh();
    }

    @Override
    public void undoTransaction() {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        TableView taTable = workspace.getTATable();
       data.updateTA(newname, oldname, oldemail);    
            
            // CLEAR THE TEXT FIELDS
           
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(newname)
                    || (label.getText().contains(newname + "\n"))
                    || (label.getText().contains("\n" + newname))) {
                        data.updataTAFromCell(label.textProperty(), newname,oldname);
                    }
                }
                
           taTable.refresh();
            AppGUI gui = app.getGUI();
       gui.getFileController().markAsEdited(gui);
        
    }
    
}
