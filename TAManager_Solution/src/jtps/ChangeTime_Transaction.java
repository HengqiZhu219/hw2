/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.TempTa;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tam.TAManagerProp;
import static tam.style.TAStyle.CLASS_ADD_TA_BUTTON;
import java.util.List;
import jtps.jTPS_Transaction;
import tam.data.TempTa;
import tam.workspace.TAWorkspace;
//import jtps.jTPS;
import tam.workspace.TAWorkspace;
//import static tam.workspace.TAController.jTPS;

/**
 *
 * @author Hengqi
 */
public class ChangeTime_Transaction implements jtps.jTPS_Transaction{
    
    TAManagerApp app;
    private final String start;
    private final String end;
    private final String oldstart;
    private final String oldend;
    private final List<TempTa> currentlist;

    
   
    
    
    /*public ChangeTime_Transaction(TAManagerApp initApp,String start, String end, List<TempTa> list){
        this.start=start;
        this.end=end;
        app = initApp;
        this.currentlist = new ArrayList();
        for(TempTa ta: list)
            this.currentlist.add(ta);
        
       
        }*/
    
        public ChangeTime_Transaction(TAManagerApp initApp,String start, String end, String lodstart, String oldends, List<TempTa> list){
        this.start=start;
        this.end=end;
        this.oldstart=lodstart;
        this.oldend=oldends;
        app = initApp;
        this.currentlist = new ArrayList();
        for(TempTa ta: list)
            this.currentlist.add(ta);
        
       
      
       
       
        }
    
    
    

    @Override
    public void doTransaction() {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
         
        String[] parts1 = start.split(":");
        String[] parts2 = end.split(":");
         workspace.resetWorkspace();
         data.initMin(parts1[1], parts2[1]);
         data.initHours(parts1[0], parts2[0]);
         workspace.reloadWorkspace(data);
         data.getTempTas().clear();
         for(TempTa ta:currentlist){
               data.addOfficeHoursReservation(ta.getDay(), ta.getTime(),ta.getName());
              
         }    
    }

    @Override
    public void undoTransaction() {
       
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        
    
        String[] parts1 = oldstart.split(":");
        String[] parts2 = oldend.split(":");
        
         workspace.resetWorkspace();
         data.initMin(parts1[1], parts2[1]);
         data.initHours(parts1[0], parts2[0]);
         workspace.reloadWorkspace(data);
         data.getTempTas().clear();
        // List<TempTa> temp;
         /*if(jTPS.getTrans().get(jTPS.getMostRecent()+1).getCurrentList()!=null)
            temp = jTPS.getTrans().get(jTPS.getMostRecent()+1).getCurrentList();
         else
             temp = jTPS.getTrans().get(jTPS.getMostRecent()).getCurrentList();*/
         for(TempTa ta: currentlist){
               data.addOfficeHoursReservation(ta.getDay(), ta.getTime(),ta.getName());
         } 
          AppGUI gui = app.getGUI();
       gui.getFileController().markAsEdited(gui);
        
         //data.getUndoList().clear();
           
    }

    
    
}
