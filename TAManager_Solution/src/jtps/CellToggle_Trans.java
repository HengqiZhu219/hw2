/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.TempTa;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tam.TAManagerProp;
import static tam.style.TAStyle.CLASS_ADD_TA_BUTTON;
import java.util.List;
import jtps.jTPS_Transaction;
import tam.data.TempTa;
//import static tam.workspace.TAController.jTPS;
import tam.workspace.TAWorkspace;


/**
 *
 * @author Hengqi Zhu
 */
public class CellToggle_Trans implements jtps.jTPS_Transaction{
    
    TAManagerApp app;
    String name;
    String cellKey;
    
    public CellToggle_Trans(TAManagerApp initApp,String name, String cellKey) {
        this.app=initApp;
        this.name=name;
        this.cellKey=cellKey;
    }

    @Override
    public void doTransaction() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       // TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        String parts[] = cellKey.split("_");
           // System.out.println(cellKey);
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
           // data.toggleTAOfficeHours(cellKey, name,parts[1],parts[0]);
            //String time = data.getCellTextProperty(0, Integer.parseInt(parts[1])).getValue();
            //String day = data.getGridHeaders().get(Integer.parseInt(parts[0]));
            data.toggleTAOfficeHours(cellKey, name,parts[1],parts[0]);
    
    
    }

    @Override
    public void undoTransaction() {
        // TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        //TAData data = (TAData)app.getDataComponent();
        //String parts[] = cellKey.split("_");
           // System.out.println(cellKey);
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            //data.toggleTAOfficeHours(cellKey, name,parts[1],parts[0]);
            
            doTransaction();
             AppGUI gui = app.getGUI();
       gui.getFileController().markAsEdited(gui);
        
         
    }

   

   

   
    
    
    
    
    
}
