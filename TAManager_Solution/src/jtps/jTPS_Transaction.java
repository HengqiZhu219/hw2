package jtps;

import java.util.List;

/**
 *
 * @author McKillaGorilla
 */
public interface jTPS_Transaction {
    public void doTransaction();
    public void undoTransaction();
 
}
