package tam.workspace;

import jtps.CellToggle_Trans;
import jtps.AddTa_Trans;
import jtps.ChangeTime_Transaction;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.TempTa;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jtps.EditTa_Trans;
import jtps.RemoveTa_Trans;
import jtps.jTPS;
import tam.TAManagerProp;
import static tam.style.TAStyle.CLASS_ADD_TA_BUTTON;

//import jtps.jTPS;
import jtps.jTPS_Transaction;


/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */

public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    boolean taTableFoucs=false;
    String initTaName;
    String initTaEmail;
    /*private static final String EMAIL_PATTERNS =
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";*/
    String tempTaName;
    ArrayList<TempTa> undoList;
    
    public static jTPS jTPS=new jTPS();
 
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This helper method should be called every time an edit happens.
     */    
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        TableView taTable = workspace.getTATable();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(!taTableFoucs){
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE)); 
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));    
        }
        else if(!validateEmail(email)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(EMAIL_INVALID_TITLE), props.getProperty(EMAIL_INVALID_MESSAGE));  
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction addTa_Trans = new AddTa_Trans(app,name,email); 
            jTPS.addTransaction(addTa_Trans);
            //data.addTA(name, email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }}
        else{
            if (name.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE)); 
            }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
            else if (email.isEmpty()) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));    
        }
            else if(!validateEmail(email)){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(EMAIL_INVALID_TITLE), props.getProperty(EMAIL_INVALID_MESSAGE)); 
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
            else if (data.containsTAEmail(email)&&data.containsTAName(name)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
            else if((tempTaName.equals(name)&&!data.containsTAEmail(email)) //change email
                    ||(!data.containsTAName(name)&&!data.containsTAEmail(email)) //change both
                    ||(!data.containsTAName(name))&&initTaEmail.equals(email)){//change name
            
             
                
                
            jTPS_Transaction editTa_Trans = new EditTa_Trans(app,tempTaName,initTaEmail,name,email); 
            jTPS.addTransaction(editTa_Trans);   
                
            /*data.updateTA(tempTaName, name, email);    
            
            // CLEAR THE TEXT FIELDS
            
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(tempTaName)
                    || (label.getText().contains(tempTaName + "\n"))
                    || (label.getText().contains("\n" + tempTaName))) {
                        data.updataTAFromCell(label.textProperty(), tempTaName,name);
                    }
                }*/
                //taTable.refresh();
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }   
            /*else if(!tempTaName.equals(name)&&data.containsTAEmail(email)){
            
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), "some one has same email");
            
            }*/
        // EVERYTHING IS FINE, ADD A NEW TA
            else if(data.containsTAName(name)){
            
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(NAME_UNIQUE_MESSAGE)); 
            }
            
        else {
           
           AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE),props.getProperty(EMAIL_UNIQUE_MESSAGE)); 
           
        }
        
        
        }
        
    }

    /**
     * This function provides a response for when the user presses a
     * keyboard key. Note that we're only responding to Delete, to remove
     * a TA.
     * 
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();
            
            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                String email = ta.getEmail();
                TAData data = (TAData)app.getDataComponent();
                jTPS_Transaction remove_Trans = new RemoveTa_Trans(app,taName,email); 
                jTPS.addTransaction(remove_Trans);
                //data.removeTA(taName);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                /*HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                    || (label.getText().contains(taName + "\n"))
                    || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                }*/
                // WE'VE CHANGED STUFF
                TextField nameTextField = workspace.getNameTextField();
                TextField emailTextField = workspace.getEmailTextField();
                nameTextField.setText("");
                emailTextField.setText("");
                markWorkAsEdited();
            }
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            TAData data = (TAData)app.getDataComponent();
            String cellKey = pane.getId();
            
            String parts[] = cellKey.split("_");
           // System.out.println(cellKey);
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            //data.toggleTAOfficeHours(cellKey, taName,parts[1],parts[0]);
          
            jTPS_Transaction cellToggleTran = new CellToggle_Trans(app,taName,cellKey); 
            jTPS.addTransaction(cellToggleTran);
            
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }

    public void handleTime() {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        
        ComboBox startTimeBox = workspace.getStartTimeBox();
        ComboBox endTimeBox = workspace.getEndTimeBox();
        //ArrayList<TempTa> tempTemp = new ArrayList();
        String startTime="";
        String endTime="";
        
        if(startTimeBox.getValue()!=null&&endTimeBox.getValue()!=null){
            if(startTimeBox.getValue().toString().contains("am")&&startTimeBox.getValue().toString().contains("12"))
                startTime = "0:00am";
            else if(startTimeBox.getValue().toString().contains("am"))
                startTime = startTimeBox.getValue().toString();
            else if(startTimeBox.getValue().toString().contains("pm")&&startTimeBox.getValue().toString().contains("12"))
                 startTime = "12:00pm";
            else if(startTimeBox.getValue().toString().contains("pm"))
                startTime = converPmToAm(startTimeBox.getValue().toString());
            
            if(endTimeBox.getValue().toString().contains("am")&&endTimeBox.getValue().toString().contains("12"))
                endTime = "0:00am";
            else if(endTimeBox.getValue().toString().contains("am"))
                endTime = endTimeBox.getValue().toString();
            else if(endTimeBox.getValue().toString().contains("pm")&&endTimeBox.getValue().toString().contains("12"))
                 endTime = "12:00pm";
            else if(endTimeBox.getValue().toString().contains("pm"))
                endTime = converPmToAm(endTimeBox.getValue().toString());  
            
            if(startTime.contains("am"))
            startTime=startTime.replace("am", "");
        else
            startTime=startTime.replace("pm", "");
        if(endTime.contains("am"))   
            endTime=endTime.replace("am", "");
        else
            endTime=endTime.replace("pm", "");
        
        String[] parts1 = startTime.split(":");
        String[] parts2 = endTime.split(":");
        
        String oldstart = data.getStartHour()+":"+"00";
        String oldend = data.getEndHour() + ":" +"00";
            
        int st = Integer.parseInt(parts1[0])*60;//+Integer.parseInt(parts1[1]);
        int et = Integer.parseInt(parts2[0])*60;//+Integer.parseInt(parts2[1]);
            
        if(st>=et){
            dialog.show(props.getProperty(TIME_ERROE_TITLE),props.getProperty(START_END));
        }
        else if(startTime.equals(oldstart)&&endTime.equals(oldend)){
            dialog.show(props.getProperty(TIME_ERROE_TITLE),props.getProperty(SAME_TIME));
        }
        
        else{
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(CONTINUE_TITLE),props.getProperty(CONTINUE_MESSAGE));
            
        
            String selection = yesNoDialog.getSelection();
            
            if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
                jTPS_Transaction changeTime_transaction = new ChangeTime_Transaction(app,startTime,endTime,oldstart,oldend,data.getTempTas()); 
                jTPS.addTransaction(changeTime_transaction);          
                markWorkAsEdited();
            }
          
        }
            
        }else{
             dialog.show(props.getProperty(TIME_ERROE_TITLE),props.getProperty(EMPTY_TIME));
        }
        
        

    }
    
    public String converPmToAm(String pm){
        //pm.replace("pm", "");
        String[] parts = pm.split(":");
        int i = Integer.parseInt(parts[0]);
        i=i+12;
        pm=Integer.toString(i)+":"+parts[1];
        //System.out.println("pm is "+pm);
        return pm;
    }
    
    public boolean validateEmail(String email){
       PropertiesManager props = PropertiesManager.getPropertiesManager();
       Pattern pattern=Pattern.compile(props.getProperty(EMAIL_PATTERN));
       Matcher matcher = pattern.matcher(email);
       return matcher.matches();
    }
    
    
    public void taTableIsClick() {
        taTableFoucs=true;
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        workspace.getAddButton().setText(props.getProperty(TAManagerProp.UPDATE_BUTTON.toString()));
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        TableView taTable=workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                //initTaName=ta.getName();
                String taEmail = ta.getEmail();
                tempTaName= taName;
                initTaEmail= taEmail;
            nameTextField.setText(taName);
            emailTextField.setText(taEmail);
            }     
                  
    }
    /*public void updateTa(String taName, String name, String email){
        TAData data = (TAData)app.getDataComponent();
        if(name.length()!=0&&email.length()!=0)
            data.updateTA(taName, name, email);      
    }*/
    
    public void handleClear() {
        taTableFoucs=false;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        workspace.getAddButton().setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        
        
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
    }
    
}