/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

/**
 *
 * @author Hengqi Zhu
 */
public class TempTa {
    private String name;
    private String time;
    private String day;

public TempTa(String n, String t, String d){
    name=n;
    time=t;
    day=d;
}
    
public String getName(){return name;}
public String getTime(){return time;}
public String getDay(){return day;}   

public String toString(){
    return name+""+time+""+day;
}

 /*public static void main(String[] args) {
     TempTa ta = new TempTa("123","123","123");
     System.out.println(ta.toString());
}*/

}
